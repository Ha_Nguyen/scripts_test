#include "inc/lib.hpp"

namespace {
	constexpr int a = 1;
	constexpr int b = 2;
}

int main() {
	static_assert(lib::Add(a, b) == 3);
	return 0;
}

